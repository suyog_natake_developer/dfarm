<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>D Farm | Home </title>
<link rel = "icon" type = "image/jpg" href = "images/a2.jpg">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<?php 
include('header.php');

//@$status = $_GET['status'];
@$ct_id = @$_GET['c_id'];  
@$sct_id = @$_GET['sc_id']; 
@$user_id = @$_SESSION['SESS_USER_ID'];
?>
<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
				<li>Kitchen</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->
<input type="hidden" value="<?php echo @$_SESSION['SESS_USER_ID'];?>" id="user_id" >
<input type="hidden" value="<?php echo @$ct_id;?>" id="ct_id" >
<input type="hidden" value="<?php echo @$sct_id;?>" id="sct_id" >

<!-- banner -->
	<div class="banner">
		<?php  include('menu.php'); ?>
		<div class="w3l_banner_nav_right">
			<div class="w3l_banner_nav_right_banner6">
				<h3>Best Deals For New Products<span class="blink_me"></span></h3>
			</div>
			<br><br>
			
	
           
        </div>
    </div> <!-- End product widget area -->
	
			
		


	<!-- top Products -->
	<div class="ads-grid">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Products
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<div id ="txt1">
			<!-- product right -->
			<div class="agileinfo-ads-display col-md-12 w3l-rightpro">
				<div class="wrapper">
				<!-- first section -->
					
				<?php
			$i =1;
			if(@$sct_id == "")
			{
				$qry = mysqli_query($con,"SELECT * FROM `products` WHERE `c_id`='$ct_id'");
			}else
			{
				$qry = mysqli_query($con,"SELECT * FROM `products` WHERE `c_id`='$ct_id' AND `sc_id`='$sct_id'");
			}
			@$cnt = mysqli_num_rows($qry); 
			
			while($row = mysqli_fetch_assoc($qry))
			  {
				  @$p_id = $row['p_id'];
			   		if(@$i == 1)
					{
						echo'<div class="product-sec1">';
					}
			?>		
					
						<div class="col-xs-3 product-men">
							<div class="men-pro-item simpleCart_shelfItem">
								<div class="men-thumb-item">
									<img src="admin/product_image/<?php echo $row['file_name'];?>" alt="">
									<div class="men-cart-pro">
										<div class="inner-men-cart-pro">
											<a href="single.php?p_id=<?php echo $row['p_id'];?>" class="link-product-add-cart">Quick View</a>
										</div>
									</div>
									<span class="product-new-top" ><?php echo $row['discount'];?>%</span>
								</div>
								<div class="item-info-product ">
									<h4>
										<a href="single.html"><?php echo $row['p_name'];?></a>
									</h4>
									<div class="info-product-price">
										<span class="item_price">&#8377; <?php echo $row['p_price'];?></span>
										<del> &#8377;<?php echo $row['disc_price'];?></del>
									</div>
									<div  class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
										<form action="#"  onsubmit="event.preventDefault();" method="post">
											<fieldset>
												<input type="hidden" name="p_id" id="p_id" value="<?php echo $row['p_id'];?>" />
												<input type="hidden" name="add" value="1" />
												<input type="hidden" name="business" value=" " />
												<input type="hidden" name="item_name" value="Zeeba Basmati Rice - 5 KG" />
												<input type="hidden" name="amount" value="950.00" />
												<input type="hidden" name="discount_amount" value="1.00" />
												<input type="hidden" name="currency_code" value="USD" />
												<input type="hidden" name="return" value=" " />
												<input type="hidden" name="cancel_return" value=" " />
												<?php
												$qrychk = mysqli_query($con,"SELECT * FROM `cart` WHERE `p_id`='$p_id' AND `user_id`='$user_id'");
												$cntchk = mysqli_num_rows($qrychk);
												if($cntchk > 0)
												{ ?>
													<input type="submit" name="submit" style="background-color:red;" value="Already in Cart" class="button" />
												<?php
												}else
												{
												?>
												<input type="submit" name="submit" onclick="addCart(<?php echo $row['p_id'];?>)"; value="Add to cart" class="button" />
												<?php
												}
												?>
											</fieldset>
										</form>
									</div>

								</div>
							</div>
						</div>
						
						
				<?php
					if(@$i == 4)
					{
						echo'<div class="clearfix"></div>
					</div>';
					}
					$i++;
					if($i == 5)
					{
						$i=1;
					}
					}
					
					if(@$i <= 4)
					{
						echo'<div class="clearfix"></div>
					</div>';
					}
				   ?>
			    

			 </div>
			</div>
			</div>
			<!-- //product right -->
		</div>
	</div>
	<!-- //top products -->

<?php include('plugin.php')?>
<?php include('footer.php');?>
</body>
</html>
<script>
	 function addCart(str) {
		// $("#txtc").load(location.href + " #txtc");
    // alert(str);
	var user_id = document.getElementById('user_id').value;
	var ct_id = document.getElementById('ct_id').value;
	var sct_id = document.getElementById('sct_id').value;
	
	if(user_id =="")
	{
		alert("Please Login First");
	}else
	{
	//var c_id = document.f1.c_id.value;
	//alert(user_id);
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","addCart.php?value="+str+"&user_id="+user_id+"&ct_id="+ct_id+"&sct_id="+sct_id,true);
        xmlhttp.send();
	
		alert("Added to Cart");
    }
    }
	 }
	 
	 
	 var timeout = setTimeout(reloadChat,2000);
	
	function reloadChat(){
		 $("#txtc").load(location.href + " #txtc",function(){
			     
				  timeout = setTimeout(reloadChat,2000);
		 });
	}
	</script>