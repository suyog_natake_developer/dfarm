<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>D Farm | Cart </title>
<link rel = "icon" type = "image/jpg" href = "images/a2.jpg">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<?php 
include('header.php');
@$status = $_GET['status'];

			@$user_id = $_SESSION['SESS_USER_ID'];
?>
<input type="hidden" value="<?php echo @$_SESSION['SESS_USER_ID'];?>" id="user_id" >
<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
				<li>Kitchen</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->

<!-- banner -->
	<div class="banner">
		<?php include('menu.php');
		include('config.php');?>
		<input type="hidden" value="<?php echo @$_SESSION['SESS_USER_ID'];?>" id="user_id" >
			<div class="w3l_banner_nav_right">
			<?php 
			@$p_id = @$_GET['p_id'];
			
			$qry2 = mysqli_Query($con,"SELECT * FROM `products` WHERE `p_id`='$p_id'");
					while($row2 = mysqli_fetch_assoc($qry2))
					{
						?>
			<div class="agileinfo_single">
				<h5><?php echo $row2['p_name'];?></h5>
				<div class="col-md-4 agileinfo_single_left">
					<img id="example" src="admin/product_image/<?php echo $row2['file_name'];?>"  alt=" " class="img-responsive" />
				</div>
				<div class="col-md-8 agileinfo_single_right">
					<div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5">
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" checked>
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
					</div>
					<div class="w3agile_description">
						<h4>Description :</h4>
						<p><?php echo $row2['p_desc'];?></p>
					</div>
					<div class="w3agile_description">
						<h4>Nutritional Value :</h4>
						<p><?php echo $row2['nutritional_value'];?></p>
					</div>
					<div class="w3agile_description">
						<h4>Banifits :</h4>
						<p><?php echo $row2['banifits'];?></p>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4>&#8377;<?php echo $row2['p_price'];?><span>&#8377;<?php echo $row2['disc_price'];?></span></h4>
						</div>
						<div class="snipcart-details agileinfo_single_right_details">
							<form action="#"  onsubmit="event.preventDefault();" method="post">
											<fieldset>
												<input type="hidden" name="p_id" id="p_id" value="<?php echo $row2['p_id'];?>" />
												<input type="hidden" name="add" value="1" />
												<input type="hidden" name="business" value=" " />
												<input type="hidden" name="item_name" value="Zeeba Basmati Rice - 5 KG" />
												<input type="hidden" name="amount" value="950.00" />
												<input type="hidden" name="discount_amount" value="1.00" />
												<input type="hidden" name="currency_code" value="USD" />
												<input type="hidden" name="return" value=" " />
												<input type="hidden" name="cancel_return" value=" " />
												<?php
												$qrychk = mysqli_query($con,"SELECT * FROM `cart` WHERE `p_id`='$p_id' AND `user_id`='$user_id'");
												$cntchk = mysqli_num_rows($qrychk);
												if($cntchk > 0)
												{ ?>
													<input type="submit" name="submit" style="background-color:red;" value="Already in Cart" class="button" />
												<?php
												}else
												{
												?>
												<input type="submit" name="submit" onclick="addCart(<?php echo $row2['p_id'];?>)"; value="Add to cart" class="button" />
												<?php
												}
												?>
											</fieldset>
										</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<?php
					}
					?>
		</div>
	</div>
<!-- //top-brands -->

		
			
		</div>
		<div class="clearfix"></div>
	</div>

	


<?php include('footer.php');  include('plugin.php');?>
</body>
</html>
<script>
	 function addCart(str) {
		 $("#txtc").load(location.href + " #txtc");
     alert(str);
	var user_id = document.getElementById('user_id').value;
	var ct_id = document.getElementById('ct_id').value;
	var sct_id = document.getElementById('sct_id').value;
	
	if(user_id =="")
	{
		alert("Please Login First");
	}else
	{
	//var c_id = document.f1.c_id.value;
	alert(user_id);
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","addCart.php?value="+str+"&user_id="+user_id+"&ct_id="+ct_id+"&sct_id="+sct_id,true);
        xmlhttp.send();
	
		alert("Added to Cart");
    }
    }
	 }
	 
	 
	 var timeout = setTimeout(reloadChat,2000);
	
	function reloadChat(){
		 $("#txtc").load(location.href + " #txtc",function(){
			     
				  timeout = setTimeout(reloadChat,2000);
		 });
	}
	</script>