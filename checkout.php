
<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>D Farm | Cart </title>
<link rel = "icon" type = "image/jpg" href = "images/a2.jpg">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<?php 
include('config.php');
@$user_id = @$_GET['SESS_USRE_ID'];
include('header.php');
?>
<input type="hidden" id="user_id" value="<?php echo @$user_id;?>">
<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
				<li>Checkout</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->
<!-- banner -->
	<div class="banner">
		<?php include('menu.php');?>
		<div class="w3l_banner_nav_right">
<!-- about -->
		<div class="privacy about" style="background-color:white;padding-left:50px;padding-right:50px;">
			<h3>Chec<span>kout</span></h3>
			
	      <div class="checkout-right">
					
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>SL No.</th>	
							<th>Product</th>
							<th>Qnty</th>
							<th>Rate</th>
							<th>Price</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$cnt = 1;
					$qry = mysqli_query($con,"SELECT * FROM `cart` WHERE `user_id`='$user_id'");
					while($row = mysqli_fetch_assoc($qry))
					{
						@$p_id = $row['p_id'];
						$qry1 = mysqli_query($con,"SELECT * FROM `products` WHERE `p_id`='$p_id'");
						while($row1 = mysqli_fetch_assoc($qry1))
						{
						?>
						<tr class="rem1">
						<td class="invert"><?php echo $cnt;?></td>
						<td class="invert"><?php echo $row1['p_name'];?></td>
						<td class="invert"><?php echo $row['qnty'];?></td>
						<td class="invert"><?php echo $row1['disc_price'];?></td>
						<td class="invert"><?php echo $row['amt'];?></td>
						</tr>
						
					<?php
						}
					}
					?>
					<?php
				$qryta = mysqli_Query($con,"SELECT SUM(amt) as amt FROM `cart` WHERE `user_id`='$user_id'");
				while($rowta = mysqli_fetch_assoc($qryta))
				{
					$t_amt = $rowta['amt'];
				}
				?>
					<tr>
					<td colspan=5>
					<b style="color:red;">Total Amount:- <?php echo $t_amt;?></b>
					</td>
					</tr>

				</tbody></table>
			</div>
			<div class="checkout-left">	
				<div class="col-md-4 checkout-left-basket">
					<h4 style="background-color:white;">Address Details</h4>
					<div class="checkout-right-basket" >
					<?php
					$qryad = mysqli_Query($con,"SELECT * FROM `address` WHERE `user_id`='$user_id'");
					$cntad = mysqli_num_rows($qryad);
					if($cntad==0)
					{
						echo'<h5>Please Add Address</h5>';
					}
					$cnt =1;
					while($rowad = mysqli_fetch_assoc($qryad))
					{
						if($cnt == 1)
						{
						echo'<input type="radio" value="'.$rowad['a_id'].'" name="a_id" checked>'.
						$rowad['streat_building'].','.
						$rowad['landmark'].','.
						$rowad['town_city'].','.
						$rowad['pincode'].',';
						echo'<br><br>';
						
						}else
						{
						echo'<input type="radio" value="'.$rowad['a_id'].'" name="a_id">'.
						$rowad['streat_building'].','.
						$rowad['landmark'].','.
						$rowad['town_city'].','.
						$rowad['pincode'].',';
						echo'<br><br>';
						}
						$cnt++;
					}
					?> <br>
					
					
					
					<input type = "button" onClick="getAdd()" style="background-color:red;color:white;" value="Add New Address" >
					<br>
					<br>
				        	<a href="payment.html" align="left">Make a Payment <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			      	</div>
				</div>
				<div id="txt1">
				
				</div>
			
				<div class="clearfix"> </div>
				
			</div>

		</div>
<!-- //about -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->


<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<div class="w3agile_newsletter_left">
				<h3>sign up for our newsletter</h3>
			</div>
			<div class="w3agile_newsletter_right">
				<form action="#" method="post">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="submit" value="subscribe now">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //newsletter -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-3 w3_footer_grid">
				<h3>information</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="events.html">Events</a></li>
					<li><a href="about.html">About Us</a></li>
					<li><a href="products.html">Best Deals</a></li>
					<li><a href="services.html">Services</a></li>
					<li><a href="short-codes.html">Short Codes</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>policy info</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="faqs.html">FAQ</a></li>
					<li><a href="privacy.html">privacy policy</a></li>
					<li><a href="privacy.html">terms of use</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>what in stores</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="pet.html">Pet Food</a></li>
					<li><a href="frozen.html">Frozen Snacks</a></li>
					<li><a href="kitchen.html">Kitchen</a></li>
					<li><a href="products.html">Branded Foods</a></li>
					<li><a href="household.html">Households</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>twitter posts</h3>
				<ul class="w3_footer_grid_list1">
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>01 day ago</i><span>Non numquam <a href="#">http://sd.ds/13jklf#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://sd.ds/1389kjklf#</a>quo nulla.</span></li>
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>02 day ago</i><span>Con numquam <a href="#">http://fd.uf/56hfg#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://fd.uf/56hfg#</a>quo nulla.</span></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			<div class="agile_footer_grids">
				<div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
					<div class="w3_footer_grid_bottom">
						<h4>100% secure payments</h4>
						<img src="images/card.png" alt=" " class="img-responsive" />
					</div>
				</div>
				<div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
					<div class="w3_footer_grid_bottom">
						<h5>connect with us</h5>
						<ul class="agileits_social_icons">
							<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#" class="dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="wthree_footer_copy">
				<p>© 2016 Grocery Store. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
		</div>
	</div>
<?php
 include('footer.php');
?>
</body>
</html>

<script>
	 function getAdd(str) {
   var user_id = document.getElementById('user_id').value;
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","getAdd.php?user_id="+user_id,true);
        xmlhttp.send();
	
		//alert("Added to Cart");
    } 
    }
	</script>