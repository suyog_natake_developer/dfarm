<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>D Farm | Home </title>
<link rel = "icon" type = "image/jpg" href = "images/a2.jpg">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<?php 
include('header.php');
@$status = $_GET['status'];
if($status == 'logout')
{
			unset($_SESSION['SESS_USER']);
			unset($_SESSION['SESS_USER_ID']);
}
?>
<!-- banner -->
	<div class="banner">
		<?php include('menu.php');?>
		<div class="w3l_banner_nav_right">
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="w3l_banner_nav_right_banner">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner1">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner2">
								<h3>upto <i>50%</i> off.</h3>
								<div class="more">
									<a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<!-- flexSlider -->
				<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
				<script defer src="js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- banner -->
	<div class="banner_bottom">
			<div class="wthree_banner_bottom_left_grid_sub">
			</div>
			<div class="wthree_banner_bottom_left_grid_sub1">
				<div class="col-md-4 wthree_banner_bottom_left">
					<div class="wthree_banner_bottom_left_grid">
						<img src="images/4.jpg" alt=" " class="img-responsive" />
						<div class="wthree_banner_bottom_left_grid_pos">
							<h4>Discount Offer <span>25%</span></h4>
						</div>
					</div>
				</div>
				<div class="col-md-4 wthree_banner_bottom_left">
					<div class="wthree_banner_bottom_left_grid">
						<img src="images/5.jpg" alt=" " class="img-responsive" />
						<div class="wthree_banner_btm_pos">
							<h3>introducing <span>best store</span> for <i>groceries</i></h3>
						</div>
					</div>
				</div>
				<div class="col-md-4 wthree_banner_bottom_left">
					<div class="wthree_banner_bottom_left_grid">
						<img src="images/6.jpg" alt=" " class="img-responsive" />
						<div class="wthree_banner_btm_pos1">
							<h3>Save <span>Upto</span> $10</h3>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
	</div>
<!-- top-brands -->
	<div class="top-brands">
		<div class="container">
			<h3>Hot Offers</h3>
			
						
	<div class="product-widget-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="single-product-widget">
                        
                        <div class="single-wid-product" style="border:outset;border-color:green;">
                            <a href="single-product.html"><img src="images/12.png" alt="" class="product-thumb"></a>
                            <h2 style="padding-top:5px;"><a href="single-product.html">fresh broccoli (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;50/-</ins> <del>&#8377;70/-</del><ins>20%</ins>
                            </div> 
				
							<div class="product-wid-rating" style="align:right;">
							   <input  style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                               <input  style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                        <div class="single-wid-product" style="border:outset;border-color:green;">
                            <a href="single-product.html"><img src="images/9.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Palak Bhaji (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                       <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/9.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Palak Bhaji (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="single-product-widget" >
                       
                        <div class="single-wid-product" style="border:outset;border-color:green;">
                            <a href="single-product.html"><img src="images/10.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Mango (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del><ins>20%</ins>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input align="right" style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                       <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/30.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Flower (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                        <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/29.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Banana (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="single-product-widget">
                        
                        <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/12.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">fresh broccoli (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;50/-</ins> <del>&#8377;70/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                        <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/11.png" alt="" class="product-thumb"></a>
                            <h2><a href="single-product.html">Apple (500 gm)</a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating">
							 
                               <input style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
                        </div>
						
                       <div class="single-wid-product">
                            <a href="single-product.html"><img src="images/9.png" alt="" class="product-thumb"></a>
                            <h2 style="padding-left:10px;"><a href="single-product.html">Palak Bhaji (500 gm)</a></h2>
                            
                            <div class="product-wid-price" style="padding-left:10px;">
                                <ins>&#8377;550/-</ins> <del>&#8377;700/-</del>
                            </div> 

							<div class="product-wid-rating" style="padding-left:10px;">
							 
                               <button style="font-style:bold;font-size:10px;background-color:red;height:25px;" type="submit" name="submit" value="Add to cart" />Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End product widget area -->
	<?php include('plugin.php');?>
		</div>
	</div>
<!-- //top-brands -->
<!-- fresh-vegetables -->
	<div class="fresh-vegetables">
		<div class="container">
			<h3>Top Products</h3>
	<section id="additional">
  <div class="container"><div class="row">
    
    <div class="block col-sm-12 widget_news">
      
      <ul>
          <li class="item">
            <span class="news_introimg">
              <a href="#">
                <img alt="" src="images/1.png">
              </a>
            </span>
            <div class="news_right">
              <a href="single-product.html">fresh broccoli (500 gm)</a>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;50/-</ins> <del>&#8377;70/-</del>
                            </div> 
				
							<div class="product-wid-rating" style="align:right;">
							 
                               <input  style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Add to cart" class="button" />
                            </div>
            </div>
          </li>

          <li class="item">
            <span class="news_introimg">
              <a href="#">
                <img alt="" src="example/author.jpg">
              </a>
            </span>
            <div class="news_right">
              <span class="news-date">April 24, 2013</span>
              <h5> 
                <a href="#">Sanctus sea sed takimata ut voluptua.</a>
              </h5>
            </div>
          </li>
        </ul>
    </div>
	
		</div>
	</div>
<!-- //fresh-vegetables -->

<?php include('footer.php');?>
</body>
</html>
