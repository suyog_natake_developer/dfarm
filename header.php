<?php include('config.php');?>
	<!--//tags -->
	
	<link href="css3/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css3/font-awesome.css" rel="stylesheet">
	<!--pop-up-box-->
	<link href="css3/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<!--//pop-up-box-->
	<!-- price range -->
	<link rel="stylesheet" type="text/css" href="css3/jquery-ui1.css">
	<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
	<!-- header-bot-->
	
	<!-- header -->
	<div class="agileits_header">
		<div class="w3l_offers">
			<a href="products.php"><i>Great Offers on D Farm</i></a>
		</div>
		
		
		<div class="product_list_header">
			<ul>
				<li class="dropdown profile_details_drop">
				<?php
					session_start();
					$user = @$_SESSION['SESS_USER'];
					if(!isset($_SESSION['SESS_USER']))
					{?>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true">Welcome User</i><span class="caret"></span></a>
					<div class="mega-dropdown-menu">
						<div class="w3ls_vegetables">
							<ul class="dropdown-menu drp-mnu">
								<li><a href="login.php">Login</a></li> 
								<li><a href="register.php">Sign Up</a></li>
							</ul>
						</div>                  
					</div>
				<?php
					}else
					{
						?>
				 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true">Welcome <?php echo $user;
?></i><span class="caret"></span></a>
					<div class="mega-dropdown-menu">
						<div class="w3ls_vegetables">
							<ul class="dropdown-menu drp-mnu">
								<li><a href="orders.php">My Orders</a></li>
								<li><a href="profile.php">Profile</a></li>
								<li><a href="index.php?status=logout">Logout</a></li> 
							</ul>
						</div>                  
					</div>
					<?php } ?>
				</li>
			</ul>
		</div>
		
		<div class="clearfix"> </div>
	</div>
<!-- script-for sticky-nav -->

	<div class="header-bot">
		<div class="header-bot_inner_wthreeinfo_header_mid">
			<!-- header-bot-->
			<div class="col-md-3 logo_agile">
				<h1>
					<a href="index.html">
						
						<img src="images/a2.jpg" alt=" " width="20%" height="40%" style="padding-left:0px;">
						<span style="font-family:Sylfaen;color:#FA1818;"><b>D Farm</b></span>
						
					</a>
				</h1>
			</div>
			<!-- header-bot -->
			<div class="col-md-9 header" >
				<!-- header lists -->
				<ul>
				<?php
					//session_start();
					$user = @$_SESSION['SESS_USER'];
					
					
					
					if(!isset($_SESSION['SESS_USER']))
					{?>
					<li>
						<a class="play-icon popup-with-zoom-anim" href="#small-dialog1">
							<span class="fa fa-map-marker" aria-hidden="true" style="color:#FA1818;"></span>Fast Delivery</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal1">
							<span class="fa fa-truck" aria-hidden="true" style="color:#FA1818;"></span>Home Delivery</a>
					</li>
					<li>
						<span class="fa fa-phone" aria-hidden="true" style="color:#FA1818;"></span>9834097959
					</li>
					<li>
						<a href="login.php" >
							<span class="fa fa-unlock-alt" aria-hidden="true" style="color:#FA1818;"></span> Sign In </a>
					</li>
					<li>
						<a href="register.php">
							<span class="fa fa-pencil-square-o" aria-hidden="true" style="color:#FA1818;"></span> Sign Up </a>
					</li>
					
					<?php
					}else
					{?>
					<li>
						<a class="play-icon popup-with-zoom-anim" href="#small-dialog1">
							<span class="fa fa-map-marker" aria-hidden="true" style="color:#FA1818;"></span>Fast Delivery</a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal1">
							<span class="fa fa-truck" aria-hidden="true" style="color:#FA1818;"></span>Home Delivery</a>
					</li>
					<li>
						<span class="fa fa-phone" aria-hidden="true" style="color:#FA1818;"></span>9834097959
					</li>
					
					<li id="txtc" >
					<?php
					@$user_id = @$_SESSION['SESS_USER_ID'];
					@$qryy = @mysqli_query($con,"SELECT * FROM `cart` WHERE `user_id` = '$user_id'");
					@$cnt = @mysqli_num_rows($qryy);
					?>
						<a href="cart.php" >
							<span class="fa fa-shopping-cart" aria-hidden="true" style="color:#FA1818;"></span>Cart(<?php echo $cnt;?>)</a>
					</li>
					<li>
						<a href="index.php?status=logout">
							<span class="fa fa-sign-out" aria-hidden="true" style="color:#FA1818;"></span>Logout </a>
					</li>
					<?php 
					
					}
					?>
				</ul>
				<!-- //header lists -->
				<!-- search -->
				<div class="agileits_search">
					<form action="#" method="post">
						<input name="Search" type="search" placeholder="How can we help you today?" required="">
						<button type="submit" class="btn btn-default" aria-label="Left Align" style="background-color:#FA1818;">
							<span class="fa fa-search" aria-hidden="true"> </span>
						</button>
					</form>
				</div>
				<!-- //search -->
				
				<div class="top_nav_right">
					<div class="wthreecartaits wthreecartaits2 cart cart box_1">
						
						<a href="cart.php">
						<button class="w3view-cart" type="submit" name="submit" value="Cart" style="background-color:#FA1818;">
								<i class="fa fa-shopping-cart" aria-hidden="true"><sup><?php echo @$cnt;?></sup></i>
						</button></a>
						
					</div>
				</div>
				<!-- //cart details -->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- shop locator (popup) -->