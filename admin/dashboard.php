<!DOCTYPE html>
<html class="no-js">
    <?php
	include('header.php');
	include('config.php');
	include('auth.php');
	
	$query1 = mysqli_query("SELECT * FROM `carret`");
		while ($row1 = mysqli_fetch_assoc($query1))
		{
			$t_carret = $row1['carret_numbers'];
		}
		
		$query11 = mysqli_query("SELECT SUM(total_bal) AS total_bal FROM `carret_once`");
		while ($row11 = mysqli_fetch_assoc($query11))
		{
			$total_bal = $row11['total_bal'];
		}
		
		$total_credited = $t_carret - $total_bal;
	?>
    <head>
        <title>Admin Home Page</title>
        
    </head>
    
    <body>

        <div class="container-fluid">
            <div class="row-fluid">
                <?php 
				include('sidebar.php');
				?>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Success</h4>
                        	Logged In successfully</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Dashboard</a> 	
	                                    </li>
	                                    
	                                </ul>
                            	</div>
                        	</div>
                    	</div>
						
					
					
					
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Low Limit Stock List</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Product Category</th>
												<th>Product Name</th>
												<th>Stock In Hand</th>
						
											</tr>
										</thead>
<tbody> 
										 <?php
        $query = mysqli_query("SELECT * FROM product");
		while ($row = mysqli_fetch_assoc($query))
		{
			if($row['p_op_stock']<=$row['low_limit'])
			{
			echo'<tr>';
			echo '<td>'.$row['p_category'].'</td>';
			echo '<td>'.$row['p_name'].'</td>';
			echo '<td>'.$row['p_op_stock'].'</td>';
			echo ' </tr>';
			}
		
		}
?>
                                            
                </tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
						
						
						
						
						
						
                    
					
					
					<div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">All ledger List</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Party</th>
												<th>Payable Amount</th>
											</tr>
										</thead>
										 <tbody> <?php
        $query = mysqli_query("SELECT * FROM party");
		while ($row = mysqli_fetch_assoc($query))
		{
			$bal_amt =0;
			$party_name = $row['pr_name'];
			
			$qrydr = mysqli_query("SELECT SUM(amt) AS amt FROM cash_debit WHERE `dealer` ='$party_name'");
			while ($rowdr = mysqli_fetch_assoc($qrydr))
			{
				$amt_dr = $rowdr['amt'];
			}
			
			$qrycr = mysqli_query("SELECT SUM(amt) AS amt FROM party_payment WHERE `dealer` ='$party_name'");
			while ($rowcr = mysqli_fetch_assoc($qrycr))
			{
				$amt_cr = $rowcr['amt'];
			}
			$bal_amt = $amt_dr - $amt_cr;
			echo"<tr>";	
			echo '<td>'.$row['pr_name'].'</td>';
			echo '<td>'.$bal_amt.'</td>';
		}
?>
                                            </tr>
                </tbody>
									</table>
                                </div>
                            </div>
                        </div>
						
						
						
						
						
						
						
						
					
                </div>
            </div>
            <hr>
            <?php
			include('footer.php');
			?>
    </body>

</html>