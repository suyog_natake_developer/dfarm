<!DOCTYPE html>
<html class="no-js">
    <?php
	include('header.php');
	include('config.php');
	?>
    <head>
        <title>Add-Sub-category</title>
        
    </head>
    
    <body>

        <div class="container-fluid">
            <div class="row-fluid">
                <?php 
				include('sidebar.php');
				?>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <?php
					     if(@$_GET['status']=='success')
						 {
							echo'<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>							
                            <h4>Add category</h4>
                        	Successfully Saved...!
						        </div>'; 
						 }
					     else{
						echo'<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>							
                            <h4>Add category</h4>
                        	Fill up information to add category
						 </div>'; }
						?>
						
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Full Screen Here</a> 	
	                                    </li>
	                                    <div class="span12">
                                    <form class="form-horizontal" action="add-sub-category-exec.php" method="post">
                                      <fieldset>
                                        <legend>Add-Sub-category</legend>
										
										<div class="control-group">
                                         
                                         <div class="control-group">
                                          <label class="control-label" for="select01">Select Category Name</label>
                                          <div class="controls">
                                            <select id="select01" class="chzn-select" required name="c_id">
                                              <option selected="selected" value="">Select Category</option>
                   
		<?php		   
		
		$query = mysqli_query($con,"SELECT * FROM `category`");
		while ($row = mysqli_fetch_assoc($query))
		{

				{
					  echo'<option value="'.$row['c_id'].'">'.$row['c_name'].'</option>';	
				}
		}
			
 ?>
                                            </select>
                                          </div>
                                        </div>
										
										
										
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Sub Category Name</label>
                                          <div class="controls">
                                            <input class="input-xsmall focused" id="sc_name"  type="text"  name="sc_name" required>
                                          </div>
                                        </div>
										
										
										<div class="control-group">
										  <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save</button>
                                          <button type="reset" class="btn">Cancel</button>
                                        </div>
                                        </div>

                                      </fieldset>
                                    </form>

						<div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">All Sub Category</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Sub Category Name</th>
												<th>Category Name</th>
												<th>Delete/Edit</th>
											</tr>
										</thead>
										 <tbody> 
		<?php
        $query = mysqli_query($con,"SELECT * FROM scategory");
		while ($row = mysqli_fetch_assoc($query))
		{
			echo"<tr>";	
			echo '<td>'.$row['sc_name'].'</td>';
			echo '<td>'.$row['c_name'].'</td>';
			echo'<td><a href="edit-category.php?c_id='.$row['c_id'].'" type="button" class="btn btn-primary"  data-toggle="tooltip" title="EDIT"><i class="icon-pencil icon-white"></i>EDIT</a>

			<a href="javascript:delete_id('.$row['c_id'].')" type="button" class="btn btn-danger" ><i class="icon-remove icon-white"></i>DELETE</a></td>';
		}
?>
                                            </tr>
                </tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
					
                                </div>
								
	                                </ul>
                            	</div>
                        	</div>	
                    </div>
                    
                </div>  
            </div>
        </div>
            <hr>
            <?php
			include('footer.php');
			?>
    </body>

</html>

<script>
function delete_id(c_id)
{
//alert(userid);
 var a = confirm("are you sure..?");
 if(a)
 {
   window.location.href='delete-category.php?c_id='+c_id;
 }else
 {
 
 }
}
</script>