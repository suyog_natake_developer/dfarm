<!DOCTYPE html>
<html class="no-js">
    <?php
	include('header.php');
	include('config.php');
	?>
    <head>
        <title>All-Product</title>
        
    </head>
    
    <body>

        <div class="container-fluid">
            <div class="row-fluid">
                <?php 
				include('sidebar.php');
				?>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <?php
					     if(@$_GET['status']=='success')
						 {
							echo'<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>							
                            <h4>All Product</h4>
                        	Successfully Saved...!
						        </div>'; 
						 }
					     else{
						echo'<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>							
                            <h4>Add Product</h4>
                        	Fill up information to add Product
						 </div>'; }
						?>
						
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Full Screen Here</a> 	
	                                    </li>
	                           
							   
							   <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">All Product</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
  									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
										<thead>
											<tr>
												<th>Product Category</th>
												<th>Product Name</th>
												<th>Product MRP</th>
												<th>Stock In Hand</th>
												<th>Delete/Edit</th>
											</tr>
										</thead>
										 <tbody> 
		<?php
        $query = mysqli_query("SELECT * FROM product");
		while ($row = mysqli_fetch_assoc($query))
		{
			echo"<tr>";	
			echo '<td>'.$row['p_category'].'</td>';
			echo '<td>'.$row['p_name'].'</td>';
			echo '<td>'.$row['p_mrp'].'</td>';
			echo '<td>'.$row['p_op_stock'].'</td>';
			echo'<td><a href="edit-product.php?p_id='.$row['p_id'].'" type="button" class="btn btn-primary"  data-toggle="tooltip" title="EDIT"><i class="icon-pencil icon-white"></i>EDIT</a>

			<a href="javascript:delete_id('.$row['p_id'].')" type="button" class="btn btn-danger" ><i class="icon-remove icon-white"></i>DELETE</a></td>';
		}
?>
                                            </tr>
                </tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
								
	                                </ul>
                            	</div>
                        	</div>	
                    </div>
                    
                </div>  
            </div>
        </div>
            <hr>
            <?php
			include('footer.php');
			?>
    </body>

</html>

<script>
function delete_id(p_id)
{
//alert(userid);
 var a = confirm("are you sure..?");
 if(a)
 {
   window.location.href='delete-product.php?p_id='+p_id;
 }else
 {
 
 }
}


function getFields(str) {
	//alert("hii");
	//alert(str);
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getFields.php?value="+str,true);
        xmlhttp.send();
        //alert(fat);
        //alert(str);
		//$("#bill").load(location.href + " #bill");
		//$("#txt1").load(location.href + " #txt1");
    }
    }
	
</script>