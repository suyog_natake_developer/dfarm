<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>login</title>
      <link rel="stylesheet" href="css/style.css">
</head>
<?php
error_reporting(0);
@ini_set('display_errors', 0);
session_start();
$session= $_GET['session'];
$no_visible_elements=true;
 
?>
<body>
  
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <h1>Sai Agro</h1><span>A/p Mahegaon, Tal-Rahuri, Dist-Ahmednagar</span>
</div>


<!-- Form Module-->
<div class="module form-module">
  <div class="toggle"><i class="fa fa-times fa-pencil"></i>
    
  </div>
  <div class="form">
    <h2>User Already Logged in...!</h2>
    <h2>Login to your account</h2>

	
    <form action="login-execute.php" method="post">
      <input type="text" name="userid" placeholder="Username"/>
      <input type="password" name="password" placeholder="Password"/>
      <button>Login</button>
    </form>
  </div>
 
  <div class="cta"><a href="#">Forgot your password?</a></div>
</div>


    <script src="js/index.js"></script>

</body>
</html>
