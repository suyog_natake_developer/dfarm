-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 15, 2019 at 05:44 PM
-- Server version: 5.1.53
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dfarm`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `a_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `streat_building` varchar(100) DEFAULT NULL,
  `landmark` varchar(100) DEFAULT NULL,
  `town_city` varchar(100) DEFAULT NULL,
  `pincode` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`a_id`, `user_id`, `mobile`, `streat_building`, `landmark`, `town_city`, `pincode`) VALUES
(1, '1', '9273991626', 'Ubale Wasti', 'Rahuuri', 'Belapur', '413715');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `crt_id` int(100) NOT NULL AUTO_INCREMENT,
  `p_id` varchar(100) DEFAULT NULL,
  `qnty` varchar(100) DEFAULT NULL,
  `uom` varchar(100) DEFAULT NULL,
  `amt` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`crt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`crt_id`, `p_id`, `qnty`, `uom`, `amt`, `user_id`, `date`, `time`) VALUES
(1, '5', '1', 'kg', '40', '1', '2019-07-15', '11:36:11'),
(2, '6', '1', 'kg', '85', '1', '2019-07-15', '11:36:13');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `c_id` int(50) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`c_id`, `c_name`, `date`, `time`) VALUES
(1, 'Vegitables', '2019-06-22', '09:53:42'),
(2, 'Fruits', '2019-06-22', '09:53:49'),
(3, 'Sprouts', '2019-06-22', '09:53:59'),
(4, 'Meat/Fish', '2019-06-22', '09:54:10'),
(5, 'Dairy Products', '2019-06-22', '09:54:18'),
(6, 'Bakery Products', '2019-06-22', '09:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `p_id` int(50) NOT NULL AUTO_INCREMENT,
  `c_id` varchar(100) DEFAULT NULL,
  `sc_id` varchar(100) DEFAULT NULL,
  `p_name` varchar(100) DEFAULT NULL,
  `p_price` varchar(100) DEFAULT NULL,
  `disc_price` varchar(100) DEFAULT NULL,
  `p_desc` varchar(100) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `low_limit` varchar(100) DEFAULT NULL,
  `p_uom` varchar(100) DEFAULT NULL,
  `folder` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `p_vat_status` varchar(100) DEFAULT NULL,
  `c_gst_per` varchar(100) DEFAULT NULL,
  `s_gst_per` varchar(50) DEFAULT NULL,
  `i_gst_per` varchar(50) DEFAULT NULL,
  `p_gst_code` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `c_id`, `sc_id`, `p_name`, `p_price`, `disc_price`, `p_desc`, `discount`, `low_limit`, `p_uom`, `folder`, `file_name`, `p_vat_status`, `c_gst_per`, `s_gst_per`, `i_gst_per`, `p_gst_code`, `date`) VALUES
(3, '1', '1', 'Demo2', '120', '100', 'Demo Product2', '', NULL, 'kg', 'product_image/', 'Fresh-Dill-iStock1.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-18'),
(4, '2', '', 'Demo3', '102', '100', 'demo 3', '10', NULL, 'kg', 'product_image/', 'Radish-Pink.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-18'),
(5, '1', '2', 'Koriender', '45', '40', 'Kobi Vegitables Best Fresh', '10', NULL, 'kg', 'product_image/', 'coriander1.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(6, '1', '2', 'Mula', '90', '85', 'Mula Fresh', '10', NULL, 'kg', 'product_image/', 'download (1).jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(7, '1', '2', 'Fresh Dill', '90', '70', 'Fresh', '20', NULL, 'kg', 'product_image/', 'Fresh-Dill-iStock1.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(8, '1', '2', 'SpringOnion', '30', '15', 'Fresh', '50', NULL, 'pcs', 'product_image/', 'springonion.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(9, '1', '2', 'Fruit Basket', '100', '90', '10', '10', NULL, 'kg', 'product_image/', 'VOFru_Basket90.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(10, '1', '2', 'Methi', '50', '25', 'Fresh', '50', NULL, 'pcs', 'product_image/', 'Methi-Kasuri.jpg', NULL, NULL, NULL, NULL, NULL, '2019-06-22'),
(11, '1', '2', 'Aalu', '10', '7', 'Fresh', '30', NULL, 'pcs', 'product_image/', '3409.png', NULL, NULL, NULL, NULL, NULL, '2019-06-22');

-- --------------------------------------------------------

--
-- Table structure for table `reg_user`
--

CREATE TABLE IF NOT EXISTS `reg_user` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(100) DEFAULT NULL,
  `u_address` varchar(100) DEFAULT NULL,
  `u_mob` varchar(100) DEFAULT NULL,
  `u_email` varchar(100) DEFAULT NULL,
  `u_password` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `reg_user`
--

INSERT INTO `reg_user` (`user_id`, `u_name`, `u_address`, `u_mob`, `u_email`, `u_password`, `date`) VALUES
(1, 'Kiran Ubale', NULL, '9273991626', 'kiranubale74@gmail.com', '123456', '2019-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `scategory`
--

CREATE TABLE IF NOT EXISTS `scategory` (
  `sc_id` int(50) NOT NULL AUTO_INCREMENT,
  `c_id` int(100) DEFAULT NULL,
  `sc_name` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  PRIMARY KEY (`sc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `scategory`
--

INSERT INTO `scategory` (`sc_id`, `c_id`, `sc_name`, `date`, `time`) VALUES
(2, 1, 'Leafy', '2019-06-22', '09:55:13'),
(3, 1, 'Daily/Regular', '2019-06-22', '09:55:26'),
(4, 1, 'Exotic', '2019-06-22', '09:56:35'),
(5, 1, 'Cut/Pilled', '2019-06-22', '09:56:49'),
(6, 1, 'Organic', '2019-06-22', '09:57:05'),
(7, 2, 'Daily/Regular', '2019-06-22', '10:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` varchar(50) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `userid` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `address`, `mobile`, `email`, `gender`, `role`, `userid`, `password`, `date`, `time`) VALUES
(1, 'Admin', 'Mahegaon', '9273991626', 'kiranubale74@gmail.com', 'male', 'Admin', 'Admin', '12345', '2016-10-07', '12:53:11');
