<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>D Farm | Cart </title>
<link rel = "icon" type = "image/jpg" href = "images/a2.jpg">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<?php 
include('header.php');
@$status = $_GET['status'];
if($status == 'logout')
{
			unset($_SESSION['SESS_USER']);
			unset($_SESSION['SESS_USER_ID']);
}
?>
<input type="hidden" value="<?php echo @$_SESSION['SESS_USER_ID'];?>" id="user_id" >
<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>|</span></li>
				<li>Kitchen</li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->

<!-- banner -->
	<div class="banner">
		<?php include('menu.php');?>
		
		

<div class="top-brands" style="background-color:white;">
	<div class="container">
			<h3>Cart</h3>
			
						
	<div class="product-widget-area" id="txt1">
        <div class="container">
            <div class="row">
				<div class="col-sm-1">
				</div>
                <div class="col-sm-4">
				
				<div class="single-product-widget">
				<?php
				 $qry1 = mysqli_query($con,"SELECT * FROM `cart` WHERE `user_id`='$user_id'");
				 $cntcrt = mysqli_num_rows($qry1);
				 if($cntcrt>0)
				 {
				 while($row1 = mysqli_fetch_assoc($qry1))
				 {  
					@$p_id = $row1['p_id'];
					$qry2 = mysqli_Query($con,"SELECT * FROM `products` WHERE `p_id`='$p_id'");
					while($row2 = mysqli_fetch_assoc($qry2))
					{
					?>
					
                        
                        <div class="single-wid-product" style="border:solid 2px;border-color:green;">
                            <a href="single-product.html"><img src="admin/product_image/<?php echo $row2['file_name'];?>" alt="" class="product-thumb"></a>
                            <h2 style="padding-top:5px;"><a href="single-product.html"><?php echo $row2['p_name'];?></a></h2>
                            
                            <div class="product-wid-price">
                                <ins>&#8377;<?php echo $row2['disc_price'];?></ins> </ins>
                            </div> 
				
							<div class="product-wid-rating" style="align:right;">
							  <select name="qnty" onchange="qchange(<?php echo $row2['p_id'];?>,this.value)"; style="font-style:bold;font-size:10px;color:black;width:50px;" >
							  <option value="<?php echo $row1['qnty'];?>"selected><?php echo $row1['qnty'];?> <?php echo $row2['p_uom'];?></option>
							  <option value="1">1 <?php echo $row2['p_uom'];?></option>
							  <option value="2">2 <?php echo $row2['p_uom'];?></option>
							  <option value="3">3 <?php echo $row2['p_uom'];?></option>
							  <option value="4">4 <?php echo $row2['p_uom'];?></option>
							  <option value="5">5 <?php echo $row2['p_uom'];?></option>
							  </select>
                               <input onclick="remove(<?php echo $row2['p_id'];?>)"; style="font-style:bold;font-size:10px;background-color:red" type="submit" name="submit" value="Remove" class="button" />
                            </div>
                        
						
                        
                    </div>
				<?php
					}
				}
				 }else
				 {
					 echo'<h3>Cart is Empty</h3>';
				 }
				?>
                 </div>   
                </div>
                <div class="col-sm-4">
				<?php
				$qryta = mysqli_Query($con,"SELECT SUM(amt) as amt FROM `cart` WHERE `user_id`='$user_id'");
				while($rowta = mysqli_fetch_assoc($qryta))
				{
					$t_amt = $rowta['amt'];
				}
				?>
                    <div class="single-product-widget" >
                       
                     <table border="1" width="100%" style="border:solid 2px;border-color:green;">
						<tr>
						<td style="padding:10px;">Total Amount:-</td>
						<td style="padding:10px;"><input type="text" value="<?php echo $t_amt;?>" disabled id="t_amt" ></td>
						</tr>
						
						<td colspan="2" align="center" style="padding:10px;">
						<a href="checkout.php"><input  style="font-style:bold;font-size:15px;background-color:red" type="submit" name="submit" value="Proceed to Checkout" class="button" /></a>
                        </td>
						
						</tr>
					 </table>
                       
						
                        
                    </div>
                </div>
                <div class="col-sm-2">
                    
                </div>
                </div>
            </div>
    </div>
    </div> <!-- End product widget area -->
	<?php include('plugin.php');?>
		</div>
	</div>
<!-- //top-brands -->

		
			
		</div>
		<div class="clearfix"></div>
	</div>

	


<?php include('footer.php');?>
</body>
</html>
<script>
	 function remove(str) {
	//alert("hi");
	var user_id = document.getElementById('user_id').value;
	//alert(user_id);
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","remove.php?value="+str+"&user_id="+user_id,true);
        xmlhttp.send();
	
		//alert("Added to Cart");
    }
    }
	 
	 function qchange(p_id,str)
	 {
		//alert("hi");
	var user_id = document.getElementById('user_id').value;
	//alert(user_id);
	   if (str == "") {
        document.getElementById("txt1").innerHTML = "";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txt1").innerHTML = xmlhttp.responseText;
            }
        }
		
        xmlhttp.open("GET","qchange.php?p_id="+p_id+"&user_id="+user_id+"&str="+str,true);
        xmlhttp.send();
	
		//alert("Added to Cart");
    }
}
	 </script>